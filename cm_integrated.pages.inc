<?php

/**
 * @file
 * Search and manage forms.
 */

/**
 * Campaign filter form.
 */
function cm_integrated_filter_form($form_state) {
  if (!isset($_SESSION['cm_integrated_manage']) || (isset($_GET['reset']) && !isset($_GET['page']))) {
    $_SESSION['cm_integrated_manage'] = array();
  }
  $session = $_SESSION['cm_integrated_manage'];

  $form['manage_filter'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Filter by criteria'),
  );
  $form['manage_filter']['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Subject'),
    '#default_value' => isset($session['title']) ? $session['title'] : '',
    '#size'          => 30,
  );
  $form['manage_filter']['campaign_status'] = array(
    '#type'          => 'select',
    '#options'       => array('' => t('- select -')) + cm_integrated_utils_status(),
    '#default_value' => isset($session['campaign_status']) ? $session['campaign_status'] : '',
    '#title'         => t('Status'),
  );
  $form['manage_filter']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Search'),
  );
  $form['manage_filter']['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset'),
  );

  return $form;
}

/**
 * Submit function for cm_integrated_filter_form().
 */
function cm_integrated_filter_form_submit($form, $form_state) {
  $fields = array('title', 'campaign_name', 'campaign_status');
  $_SESSION['cm_integrated_manage'] = array();

  if ($form_state['values']['op'] == t('Reset')) {
    return;
  }

  foreach ($fields as $fld) {
    if ($form_state['values'][$fld] || $form_state['values'][$fld] == '0') {
      $_SESSION['cm_integrated_manage'][$fld] = $form_state['values'][$fld];
    }
  }
}

/**
 * Page callback for campaign listing/searching.
 */
function cm_integrated_manage() {
  drupal_add_css(drupal_get_path('module', 'cm_integrated') . '/css/cm_integrated.css');

  $output = drupal_get_form('cm_integrated_filter_form');
  $where = array();

  if (isset($_SESSION['cm_integrated_manage']) && !empty($_SESSION['cm_integrated_manage'])) {
    $where = $_SESSION['cm_integrated_manage'];
  }

  $campaigns = cm_integrated_db_get_campaign(10, $where);

  $output .= "<div class='clear'> </div>";
  $all_status = cm_integrated_utils_status();
  $status_by_name = array_flip(cm_integrated_utils_status(NULL, TRUE));

  $header = array(
    t('Subject'),
    t('From'),
    t('Reply To'),
    t('Created Date'),
    t('Status'),
    t('Scheduled/Sent Date'),
    t('Actions'),
  );

  $rows   = array();
  foreach ($campaigns as $nid => $campaign) {
    $status_info = $all_status[$campaign['campaign_status']];
    if ($campaign['is_deleted']) {
      $status_info .= "&nbsp; (" . t('Deleted') . ")";
    }
    $rows[] = array(
      l($campaign['title'], "node/{$campaign['nid']}"),
      $campaign['from_name'] . ', ' . $campaign['from_email'],
      $campaign['reply_to'],
      format_date($campaign['created']),
      array('data' => $status_info, 'class' => "cm_integrated-status-{$status_by_name[$campaign['campaign_status']]}"),
      isset($campaign['send_date']) ? format_date(strtotime($campaign['send_date'])) : t('N/A'),
      cm_integrated_manage_actions($campaign),
    );
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'data'    => t('No newsletters found!'),
        'colspan' => '7',
        'class'   => 'message',
      ),
    );
  }

  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 10, 0);

  return $output;
}

/**
 * Function to build action links for campaign.
 */
function cm_integrated_manage_actions($campaign) {
  $links = '';
  if (isset($campaign['campaign_status']) && !$campaign['is_deleted'] && $campaign['campaign_status'] != cm_integrated_utils_status('n/a')) {
    $links = l(t('Manage'), "node/{$campaign['nid']}/cm_integrated");
  }

  return $links;
}
