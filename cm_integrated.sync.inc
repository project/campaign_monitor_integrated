<?php

/**
 * @file
 * Campaign Monitor - drupal synchronization functions.
 */

/**
 * Synchronize user lists.
 *
 * @return array
 *   Messages generated in terms of db processing,
 *   Api status
 */
function cm_integrated_sync_lists() {
  // Get all lists from Campaign Monitor.
  list($cm_lists, $status) = cm_integrated_get_api_lists();
  $messages = array();

  if (!$status['success']) {
    // @TODO: add watchdog.
    return array($messages, $status);
  }

  // Get all lists from db.
  $db_lists = cm_integrated_db_get_lists();

  // Delete all records if no lists found on Campaign Monitor.
  if (!empty($db_lists) && empty($cm_lists)) {
    db_query("DELETE FROM {cm_integrated_lists}");
  }

  if (empty($cm_lists)) {
    $messages[] = t('No user list found!');
    return array($messages, $status);
  }

  $delete_lists = array();
  foreach (array_keys($db_lists) as $lid) {
    if (!isset($cm_lists[$lid])) {
      $delete_lists[$lid] = $lid;
    }
  }

  // Delete not found lists.
  if (!empty($delete_lists)) {
    db_query("DELETE FROM {cm_integrated_lists} WHERE lid IN (" . db_placeholders($delete_lists, 'varchar') . ")", $delete_lists);
    $messages[] = t('%1 user list has been deleted.', array('%1' => count($delete_lists)));
  }

  $addcount = 0;
  foreach ($cm_lists as $lid => $lname) {
    if (!isset($db_lists[$lid])) {
      db_query("INSERT INTO {cm_integrated_lists} (lid, list_name) VALUES ('%s', '%s')", $lid, $lname);
      $addcount++;
    }
  }

  // Insert new user lists.
  if ($addcount > 0) {
    $messages[] = t('%1 user list has been added.', array('%1' => $addcount));
  }
  else {
    $messages[] = t('No any new user list added.');
  }
  return array($messages, $status);
}

/**
 * Synchronize segments.
 *
 * @return array
 *   Messages generated interms of db processing,
 *   Api status.
 */
function cm_integrated_sync_segments() {
  // Get all segments from Campaign Monitor.
  list($cm_segments, $status) = cm_integrated_get_api_segments();
  $messages = array();

  if (!$status['success']) {
    // @TODO: add watchdog.
    return array($messages, $status);
  }

  // Get all segments from db.
  $db_segments = cm_integrated_db_get_segments();

  // Delete all records if no segment found on Campaign Monitor.
  if (!empty($db_segments) && empty($cm_segments)) {
    $query = db_query("DELETE * FROM {cm_integrated_segments}");
  }

  if (empty($cm_segments)) {
    $messages[] = t('No segments found!');
    return array($messages, $status);
  }

  $delete_segments = array();
  foreach (array_keys($db_segments) as $sid) {
    if (!isset($cm_segments[$sid])) {
      $delete_segments[$sid] = $sid;
    }
  }

  // Delete not found segments.
  if (!empty($delete_segments)) {
    db_query("DELETE FROM {cm_integrated_segments} WHERE sid IN (" . db_placeholders($delete_segments, 'varchar') . ")", $delete_segments);
    $messages[] = t('%1 segments has been deleted.', array('%1' => count($delete_segments)));
  }

  $addcount = 0;
  foreach ($cm_segments as $sid => $segment) {
    if (!isset($db_segments[$sid])) {
      db_query("INSERT INTO {cm_integrated_segments} (sid, lid, segment_name) VALUES ('%s', '%s', '%s')", $sid, $segment['ListID'], $segment['Title']);
      $addcount++;
    }
  }

  // Insert new user lists.
  if ($addcount > 0) {
    $messages[] = t('%1 segments has been added.', array('%1' => $addcount));
  }
  else {
    $messages[] = t('No any new segment added.');
  }
  return array($messages, $status);
}

/**
 * Synchronize campaigns.
 *
 * @return array
 *   Messages generated in terms of db processing,
 *   Api status.
 */
function cm_integrated_sync_campaigns() {
  // Get all send campaign from Campaign Monitor.
  list($cm_campaigns, $status) = cm_integrated_api_get_sent_camapigns();
  $messages = array();

  if (!$status['success']) {
    // @TODO: add watchdog.
    return array($messages, $status);
  }

  if (!empty($cm_campaigns)) {
    $messages[] = t('Updated sent campaigns (if any).');
  }
  else {
    $messages[] = t('No any sent campaigns found!');
  }

  // Synchronize send campaigns.
  _cm_integrated_sync_campaigns_sent($cm_campaigns);

  $cm_campaigns = $status = array();

  // Get all draft campaign from Campaign Monitor.
  list($cm_campaigns, $status) = cm_integrated_api_get_draft_camapigns();

  if (!$status['success']) {
    // @TODO: add watchdog.
    return array($messages, $status);
  }

  if (!empty($cm_campaigns)) {
    $messages[] = t('Updated draft campaigns (if any).');
  }
  else {
    $messages[] = t('No any draft campaigns found!');
  }

  // Synchronize send campaigns.
  _cm_integrated_sync_campaigns_draft($cm_campaigns);

  return array($messages, $status);
}

/**
 * Helper function to synchronize send campaign.
 *
 * @see cm_integrated_sync_campaigns()
 */
function _cm_integrated_sync_campaigns_sent($cm_campaigns) {
  $draft = cm_integrated_utils_status('draft');
  $scheduled = cm_integrated_utils_status('scheduled');
  $send = cm_integrated_utils_status('sent');

  if (empty($cm_campaigns)) {
    // If no campaign found on CM ( may be deleted from CM ),
    // mark all records as deleted.
    db_query("UPDATE {cm_integrated_campaigns} SET is_deleted = 1 WHERE campaign_status = %d", $send);
    return;
  }

  // Mark not found campaign records as deleted.
  db_query("UPDATE {cm_integrated_campaigns} SET is_deleted = 1 WHERE cid IS NOT NULL AND campaign_status = %d AND cid NOT IN (" . db_placeholders(array_keys($cm_campaigns), 'varchar') . ")", array_merge((array) $send, array_keys($cm_campaigns)));

  // Update campaign status.
  db_query("UPDATE {cm_integrated_campaigns} SET campaign_status = %d WHERE campaign_status <> %d AND cid IN (" . db_placeholders(array_keys($cm_campaigns), 'varchar') . ")", array_merge((array) $send, (array) $send, array_keys($cm_campaigns)));

  // Update statistic of the campaign.
  cm_integrated_db_update_statistics($cm_campaigns, TRUE);
}

/**
 * Helper function to synchronize draft campaign.
 *
 * @see cm_integrated_sync_campaigns()
 */
function _cm_integrated_sync_campaigns_draft($cm_campaigns) {
  $draft = cm_integrated_utils_status('draft');
  $scheduled = cm_integrated_utils_status('scheduled');

  if (empty($cm_campaigns)) {
    // If no campaign found on CM ( may be deleted from CM ),
    // mark all records as deleted.
    db_query("UPDATE {cm_integrated_campaigns} SET is_deleted = 1 WHERE campaign_status = %d", $draft);
    return;
  }

  // Mark not found campaign records as deleted.
  db_query("UPDATE {cm_integrated_campaigns} SET is_deleted = 1 WHERE campaign_status = %d AND cid IS NOT NULL AND cid NOT IN (" . db_placeholders(array_keys($cm_campaigns), 'varchar') . ")", array_merge((array) $draft, array_keys($cm_campaigns)));

  // Update statistic of the campaign.
  cm_integrated_db_update_statistics($cm_campaigns, TRUE);
}

/**
 * Function to synchronize statistics for specific campaign.
 */
function cm_integrated_sync_statistics($campaign) {
  $status_by_name = cm_integrated_utils_status(NULL, TRUE);
  if (empty($campaign['cid']) || $campaign['is_deleted'] || !$campaign['is_validated'] || ($campaign['campaign_status'] && !in_array($campaign['campaign_status'], array($status_by_name['sent'], $status_by_name['scheduled'])))) {
    return;
  }

  $synchronize   = FALSE;
  $update_status = FALSE;
  if ($campaign['campaign_status'] == $status_by_name['scheduled']) {
    if (date('YmdHis') > str_replace(array('-', '/', ' ', ':'), '', $campaign['send_date'])) {
      $synchronize   = TRUE;
      $update_status = TRUE;
    }
    else {
      return;
    }
  }

  // Set time intervals(in hrs).
  $time_interval = 2;

  if (empty($campaign['stat_update_date'])) {
    $synchronize = TRUE;
  }
  else {
    $last_date = str_replace(array('-', '/', ' ', ':'), '', $campaign['stat_update_date']);
    $current_date = date('YmdHis');
    if (($current_date - $last_date) >= ($time_interval * 60 * 60)) {
      $synchronize = TRUE;
    }
  }

  if (!$synchronize) {
    return;
  }

  list($summary, $status) = cm_integrated_api_campaign_summary($campaign['cid']);
  if (!$status['success'] || empty($summary)) {
    // @TODO: add watchdog.
    return;
  }

  if ($update_status) {
    db_query("UPDATE {cm_integrated_campaigns} SET campaign_status = %d WHERE cid = '%s'", $status_by_name['sent'], $campaign['cid']);
  }
  cm_integrated_db_update_campaign_statistics($summary);
}
