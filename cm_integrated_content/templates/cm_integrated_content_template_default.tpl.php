<?php
/**
 * @file
 * cm_integrated_content_template_default.tpl.php
 *
 * Theme implementation to display a teaser view
 * for newsletter.
 *
 * Available variables:
 * $nodes list of full node objects
 */
?>
<div class="cmi_content_wrapper">
  <?php
    // $nodes: array of all selected node objects.
  foreach ($nodes as $node) :
    print '<div class="cmi_content_title"><strong>' . $node->title . '</strong></div>';
    print '<div class="cmi_content_content">' . $node->teaser . '</div>';
    print '<div class="cmi_content_readmore">' . l(t('Read More'), "node/{$node->nid}") . '</div>';
  endforeach; ?>
</div>
