
Campaign Monitor Integrated
=========
The Campaign Monitor Integrated module makes it possible to create newsletters
and synchronize these with your Campaign Monitor account.

Installation
============
1. Download the Campaign Monitor API wrapper files from 
   https://github.com/campaignmonitor/createsend-php and place
   all the files inside sites/all/libraries/campaignmonitor_createsend

2. Install & enable the module.

3. Fill in your API keys at admin/settings/cm_settings
   Choose which content type you want to use for newsletter nodes.

4. Synchronize user lists (<drupalroot>/admin/settings/cm_settings/sync)
   with Campaign Monitor.

5. Setup a cron ( if not already ) to automatically camapaign synchronization.
