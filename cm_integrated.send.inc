<?php

/**
 * @file
 * View and send campaign.
 */

/**
 * Page callback for campaign scheduled/sent information.
 */
function cm_integrated_send($node) {
  drupal_add_css(dirname(__FILE__) . "/css/cm_integrated.css");

  // Get campaign details of node.
  $campaign = cm_integrated_db_get_campaign_details($node->nid, TRUE, FALSE, TRUE);

  if (empty($campaign) || !$campaign['is_validated']) {
    return "<div class='message warning'>" . t('Newsletter was not created for this content. Please update this content with valid input.') . "</div>";
  }
  elseif (!empty($campaign) && $campaign['is_deleted']) {
    return  "<div class='message warning'>" . t('Newsletter composed for this content was deleted. Please update this content to create new.') . "</div>";
  }

  require_once dirname(__FILE__) . "/cm_integrated.sync.inc";
  cm_integrated_sync_statistics($campaign);

  $status_by_name = cm_integrated_utils_status(NULL, TRUE);
  $output         = _cm_integrated_send_node($campaign);

  // If campaign status is draft attach campaign send form.
  if ($campaign['campaign_status'] && $campaign['campaign_status'] == $status_by_name['draft']) {
    // Synchronize campaign here if we want preview url,
    // before running cron.
    // if (!$status_by_name['preview_url']) {
    // cm_integrated_sync_campaigns( );
    // }
    $output .= "<div class='clear'> </div>";
    $output .= drupal_get_form('cm_integrated_send_form', $node->nid);
  }
  else {
    $output .= _cm_integrated_send_statistics($campaign);
  }
  return $output;
}

/**
 * Campaign send form.
 */
function cm_integrated_send_form($form_state, $nid) {
  global $user;

  $form['cm_send'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Schedule Campaign'),
  );
  $form['cm_send']['nid'] = array(
    '#type'     => 'hidden',
    '#value'    => $nid,
    '#required' => TRUE,
  );
  $form['cm_send']['send_date'] = array(
    '#type'            => 'date_select',
    '#default_value'   => date('Y-m-d H:i:s'),
    '#date_year_range' => '0:+3',
    '#title'           => t('Send Date'),
    '#required'        => TRUE,
  );
  $form['cm_send']['send_email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Confirmation Email'),
    '#default_value' => isset($user->mail) ? $user->mail : NULL,
    '#required'      => TRUE,
    '#size'          => 25,
  );
  $form['cm_send']['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save configuration'),
    '#prefix' => '<br/>',
  );

  return $form;
}

/**
 * Validate function for cm_integrated_send_form().
 */
function cm_integrated_send_form_validate($form, &$form_state) {
  // Validate email field.
  $form_state['values']['send_email'] = trim($form_state['values']['send_email']);
  if (!valid_email_address($form_state['values']['send_email'])) {
    form_set_error('send_email', t('Please enter valid confirmation email id.'));
  }

  // Validate date field.
  if (!date_is_valid($form_state['values']['send_date'])) {
    form_set_error('send_date', t('Please enter valid send date.'));
    return;
  }
  $diff = strtotime($form_state['values']['send_date']) - strtotime(date('Y-m-d H:i:s'));

  // Send date >= 1 min.
  if ($diff < 120) {
    form_set_error('send_date', t('Send date should at least greator than 1 min of current date.'));
  }
}

/**
 * Submit function for cm_integrated_send_form().
 */
function cm_integrated_send_form_submit($form, $form_state) {
  // Get campaign details.
  $campaign = cm_integrated_db_get_campaign_details($form_state['values']['nid']);

  if (empty($campaign) || empty($campaign['cid'])) {
    drupal_set_message(t("Couldn't find valid campaign. Please try to resend this campaign."), 'warning');
    return;
  }

  $params = array(
    'nid'        => $form_state['values']['nid'],
    'cid'        => $campaign['cid'],
    'send_date'  => $form_state['values']['send_date'],
    'send_email' => $form_state['values']['send_email'],
  );

  // Send Camapaign.
  list($cmapaign, $status) = cm_integrated_api_campaign_send($params);

  if ($status['success']) {
    // Update campaign status as scheduled.
    cm_integrated_db_campaign_send($form_state['values']['nid'], $form_state['values']['send_date']);
    drupal_set_message(t('Newsletter has been scheduled for send.'));
  }
  else {
    if ($status['error']) {
      $errormsg = t($status['error']->Message);
      drupal_set_message($errormsg . '.<br/>' . t('Please contact administator for further assesment.'), 'error');
    }
    else {
      drupal_set_message(t('Unknown error found! Please try to resend this campaign.'), 'error');
    }
  }
}

/**
 * Helper function to build camapign scheduled/sent information.
 */
function _cm_integrated_send_node($campaign) {
  $status         = cm_integrated_utils_status();
  $status_by_name = array_flip(cm_integrated_utils_status(NULL, TRUE));

  $rows = array();
  $rows[] = array(
    array(
      'data' => t('Campaign Name'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['campaign_name'],
      'class' => 'cm_integrated-value',
    ),
  );
  $rows[] = array(
    array(
      'data' => t('Created Date'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => format_date($campaign['created']),
      'class' => 'cm_integrated-value',
    ),
  );
  $rows[] = array(
    array(
      'data' => t('From'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['from_name'],
      'class' => 'cm_integrated-value',
    ),
  );
  $rows[] = array(
    array(
      'data' => t('Reply To'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['reply_to'],
      'class' => 'cm_integrated-value',
    ),
  );
  $rows[] = array(
    array(
      'data' => t('User List(s)'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => implode('<br/>', $campaign['lists']),
      'class' => 'cm_integrated-value',
    ),
  );
  $rows[] = array(
    array(
      'data' => t('Status'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $status[$campaign['campaign_status']],
      'class' => "cm_integrated-value cm_integrated-status-{$status_by_name[$campaign['campaign_status']]}",
    ),
  );

  if ($campaign['send_date']) {
    $rows[] = array(
      array(
        'data' => t('Scheduled/Sent Date'),
        'class' => 'cm_integrated-label',
      ),
      array(
        'data' => format_date(strtotime($campaign['send_date'])),
        'class' => 'cm_integrated-value',
      ),
    );
  }

  if ($campaign['campaign_status'] && $status_by_name[$campaign['campaign_status']] != 'n/a') {
    if ($campaign['preview_url']) {
      $rows[] = array(
        array(
          'data' => '&nbsp;',
          'class' => 'cm_integrated-label',
        ),
        array(
          'data' => l(t('Preview'), $campaign['preview_url'], array('attributes' => array('target' => '_blank'))),
          'class' => 'cm_integrated-value',
        ),
      );
    }
  }

  $output  = '<fieldset><legend>' . t('Newsletter Detail') . '</legend>';
  $output .= theme('table', array(), $rows);
  $output .= '</fieldset>';
  return $output;
}

/**
 * Helper function to build static information for campaign.
 */
function _cm_integrated_send_statistics($campaign) {
  $rows = array();

  $rows[] = array(
    array(
      'data' => t('Recipients'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['recipients'],
      'class' => 'cm_integrated-value',
    ),
  );

  $rows[] = array(
    array(
      'data' => t('Total Opened'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['opened'],
      'class' => 'cm_integrated-value',
    ),
  );

  $rows[] = array(
    array(
      'data' => t('Clicks'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['clicks'],
      'class' => 'cm_integrated-value',
    ),
  );

  $rows[] = array(
    array(
      'data' => t('Unsubscribed'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['unsubscribed'],
      'class' => 'cm_integrated-value',
    ),
  );

  $rows[] = array(
    array(
      'data' => t('Bounced'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['bounced'],
      'class' => 'cm_integrated-value',
    ),
  );

  $rows[] = array(
    array(
      'data' => t('Unique Opened'),
      'class' => 'cm_integrated-label',
    ),
    array(
      'data' => $campaign['unique_opened'],
      'class' => 'cm_integrated-value',
    ),
  );

  $output  = '<fieldset><legend>' . t('Newsletter Statistics') . '</legend>';
  $output .= theme('table', array(), $rows);
  $output .= '</fieldset>';
  return $output;
}
