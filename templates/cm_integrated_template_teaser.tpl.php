<?php
/**
 * @file
 * cm_integrated_template_teaser.tpl.php
 *
 * Theme implementation to display a teaser view
 * for newsletter.
 *
 * Available variables:
 * $node full node object
 */
?>
<div id="cm_newsletter_content_wrapper_teaser">
  <h1 class="cm_newsletter_title"><?php print $node->title; ?></h1>
  <div class="cm_newsletter_content">
    <?php echo $node->teaser; ?>
  </div>
</div>
