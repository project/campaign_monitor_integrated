<?php

/**
 * @file
 * Node selection to create campaign.
 */

/**
 * Function to build a form for node selections and to arrange the nodes.
 */
function cm_integrated_content_select($form_state) {
  // Use multistep form to select nodes and to arrange the nodes.
  if (is_null($form_state['storage'])) {
    $content_types = array();
    foreach (node_get_types() as $name => $type) {
      $content_types[$name] = $type->name;
    }

    // Build node filters.
    $where = array();
    if (!empty($form_state['post'])) {
      if (!empty($form_state['post']['title'])) {
        $where['title'] = $form_state['post']['title'];
      }
      if (!empty($form_state['post']['type'])) {
        $where['type'] = $form_state['post']['type'];
      }
      if (isset($form_state['post']['status']) && is_numeric($form_state['post']['status'])) {
        $where['status'] = $form_state['post']['status'];
      }
    }

    $form['search'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Filters'),
      '#collapsible' => TRUE,
      '#collapsed'   => empty($where) ? TRUE : FALSE,
    );
    $form['search']['title'] = array(
      '#type'        => 'textfield',
      '#title'       => t('Title'),
      '#description' => t('Title like'),
    );
    $form['search']['type'] = array(
      '#type'        => 'select',
      '#title'       => t('Type'),
      '#description' => t('Content type'),
      '#multiple'    => TRUE,
      '#options'     => $content_types,
      '#size'        => 5,
    );
    $form['search']['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => array(
        ''  => t('-- Any --'),
        '1' => t('published'),
        '0' => t('not published'),
      ),
    );
    $form['search']['search'] = array(
      '#type' => 'button',
      '#value' => t('Search'),
    );
    $form['campaign_title'] = array(
      '#type'        => 'textfield',
      '#title'       => t('Title'),
      '#description' => t('Newsletter little'),
    );
    $form['step'] = array(
      '#type'  => 'hidden',
      '#value' => 1,
    );

    // Find nodes with respect to the provided filters.
    $nodes = _cm_integrated_find_nodes($where);
    foreach ($nodes as $node) {
      $form['nid_' . $node->nid] = array(
        '#type'         => 'checkbox',
        '#return_value' => $node->nid,
      );
    }

    // Get enabled templates.
    $enabled_templates = variable_get('cm_integrated_content_templates', array('default', 'list'));

    // Get all template files.
    $all_templates = _cm_integrated_content_template_files();

    $templates = array('' => t('-- select --'));
    foreach ($all_templates as $name => $title) {
      if (in_array($name, $enabled_templates)) {
        $templates[$name] = $title;
      }
    }
    $form['template'] = array(
      '#type'    => 'select',
      '#title'   => t('Select Template'),
      '#options' => $templates,
    );
    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => t('Arrange nodes'),
    );
  }
  else {
    // Provide sorting interface to the selected nodes.
    drupal_add_tabledrag('cm_integrated_nodes', 'order', 'sibling', 'cm_weight');
    $nodes  = $form_state['storage']['nodes'];

    $count          = 0;
    $weigth_options = array();
    for ($i = 0; $i <= 25; $i++) {
      $weigth_options[] = $i;
    }

    foreach ($nodes as $nid) {
      $form['weight_' . $nid] = array(
        '#type'          => 'select',
        '#title'         => t('Weight'),
        '#options'       => $weigth_options,
        '#default_value' => $count,
        '#attributes'    => array('class' => 'cm_weight'),
      );
      $count++;
    }

    $form['step'] = array(
      '#type'  => 'hidden',
      '#value' => 2,
    );

    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => t('Create newsletter'),
    );
  }

  return $form;
}

/**
 * Validate function for cm_integrated_content_select().
 */
function cm_integrated_content_select_validate($form, &$form_state) {
  if (($form_state['values']['op'] == t('Arrange nodes')) && ($form_state['values']['step'] == 1)) {
    if (!trim($form_state['values']['campaign_title'])) {
      form_set_error('campaign_title', t('Title field is required.'));
    }
    else {
      // Create dummy node object to check duplicate
      // campaign.
      $node = new stdClass();
      $node->title = $form_state['values']['campaign_title'];
      if (_cm_integrated_is_campaign_exits($node)) {
        form_set_error('campaign_title', t('Campaign already exists with this title! Please choose another title.'));
      }
    }

    if (!$form_state['values']['template']) {
      form_set_error('template', t('Please select a template.'));
    }

    $found = FALSE;
    foreach ($form_state['values'] as $field => $value) {
      if (ereg('nid_', $field) && $value != 0) {
        $found = TRUE;
        break;
      }
    }
    // At least one node to be selected.
    if (!$found) {
      form_set_error('', t('Please select at least one node.'));
    }
  }
}

/**
 * Submit function for cm_integrated_content_select().
 */
function cm_integrated_content_select_submit($form, &$form_state) {
  if ($form_state['values']['step'] == 1) {
    // Get search fields.
    $form_state['storage']['search'] = $form_state['values']['title'];
    $form_state['storage']['search'] = $form_state['values']['type'];
    $form_state['storage']['search'] = $form_state['values']['status'];

    // Get selected nodes.
    foreach ($form_state['values'] as $field => $value) {
      if (ereg('nid_', $field) && $value != 0) {
        $nodes[] = $value;
      }
    }

    $form_state['storage']['nodes'] = $nodes;
    $form_state['storage']['campaign_title'] = $form_state['values']['campaign_title'];
    $form_state['storage']['template'] = $form_state['values']['template'];

    // Follow to the next step.
    $form_state['storage']['step'] = 2;
    $form_state['rebuild'] = TRUE;
  }
  else {
    foreach ($form_state['values'] as $key => $value) {
      if (ereg('weight_', $key)) {
        $nid = explode('_', $key);
        $nid = $nid[1];
        $nodes[$nid] = $value;
      }
    }

    asort($nodes);
    $nodes = array_keys($nodes);

    // Create campaign from selected nodes.
    _cm_integrated_content_campaign($nodes, $form_state['storage']['campaign_title'], $form_state['storage']['template']);
  }
}
