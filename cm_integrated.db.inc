<?php

/**
 * @file
 * Database calls for retrive/create/update userlists, campaigns.
 */

/**
 * Fetch lists from database.
 *
 * @param int $nid
 *   Node Id.
 *
 * @return array
 *   List of lists.
 */
function cm_integrated_db_get_lists($nid = NULL) {
  $lists = array();
  $query = " SELECT * FROM {cm_integrated_lists} ";
  if ($nid) {
    $query = "SELECT * FROM {cm_integrated_campaign_lists} cl LEFT JOIN {cm_integrated_lists} nl ON nl.lid = cl.lid WHERE cl.nid = %d";
  }

  $result = db_query($query, $nid);
  while ($list = db_fetch_array($result)) {
    $lists[$list['lid']] = $list['list_name'];
  }

  return $lists;
}

/**
 * Fetch segments from database.
 *
 * @param bool $detail
 *   Return details or just list.
 *
 * @return array
 *   List of segments.
 */
function cm_integrated_db_get_segments($detail = FALSE) {
  $segments = array();
  $query    = db_query("SELECT * FROM {cm_integrated_segments}");
  while ($segment = db_fetch_array($query)) {
    if ($detail) {
      $segments[$segment['sid']] = $segment;
    }
    else {
      $segments[$segment['sid']] = $segment['segment_name'];
    }
  }
  return $segments;
}

/**
 * Create campaign record in database.
 *
 * @param array $params
 *   Campaign to be create.
 */
function cm_integrated_db_create_campaign(&$params) {
  $node_id = $params['nid'];
  if (!$node_id) {
    return;
  }

  // Delete if previous record is exist.
  cm_integrated_db_delete_campaign(node_load($node_id));

  $campaign = new stdClass();
  $campaign->nid = $node_id;
  foreach ($params as $field => $value) {
    if (!empty($value) || $value === 0) {
      $campaign->$field = $value;
    }
  }

  // Send date is NULL.
  // Insert campaign record.
  drupal_write_record('cm_integrated_campaigns', $campaign);

  if ($params['cid']) {
    // Put statistic record entry.
    db_query("INSERT INTO {cm_integrated_statistics} ( cid ) VALUES ('%s')", $params['cid']);
  }

  if (!empty($params['lists'])) {
    // Insert records of user list for the node.
    foreach ($params['lists'] as $lid) {
      db_query("INSERT INTO {cm_integrated_campaign_lists} VALUES (%d, '%s', NULL)", $node_id, $lid);
    }
  }

  if (!empty($params['segments'])) {
    // Insert records of segments for the node.
    $segments     = $args = array();
    $db_segements = cm_integrated_db_get_segments(TRUE);
    foreach ($params['segments'] as $sid) {
      db_query("INSERT INTO {cm_integrated_campaigns_lists} VALUES (%d, '%s', '%s')", $node_id, $db_segements[$sid]['lid'], $sid);
    }
  }
}

/**
 * Fetch campaign record from database.
 *
 * @param int $pager
 *   Number of records
 * @param string $where
 *   Extra where clause
 * @param bool $only_active
 *   Only published nodes?
 * @param bool $return_lists
 *   Return lists?
 * @param bool $return_segements
 *   Return segements?
 * @param bool $return_stat
 *   Return statistics?
 *
 * @return array
 *   Campaign records.
 */
function cm_integrated_db_get_campaign($pager = 50, $where = NULL, $only_active = FALSE, $return_lists = TRUE, $return_segements = FALSE, $return_stat = FALSE) {
  $campaigns = $where_clause = array();
  $query = "SELECT * FROM {cm_integrated_campaigns} c INNER JOIN {node} n ON n.nid = c.nid ";

  // Include statistics.
  if ($return_stat) {
    $query .= " LEFT JOIN {cm_integrated_statistics} cns ON cns.cid = c.cid ";
  }

  list($where_clause, $args) = _cm_integrated_db_build_where($where);

  if ($only_active) {
    $where_clause[] = "n.is_active = %d";
    $args[] = 1;
  }

  if (!empty($where_clause)) {
    $query .= " WHERE " . implode(' AND ', $where_clause);
  }
  $query .= " ORDER BY n.created DESC";

  if ($pager) {
    // In case of pager.
    $result = pager_query($query, $pager, 0, NULL, $args);
  }
  else {
    $result = db_query($query, $args);
  }

  while ($campaign = db_fetch_array($result)) {
    $campaigns[$campaign['nid']] = $campaign;
  }

  // Include list and segments.
  if (($return_segements || $return_lists) && !empty($campaigns)) {
    $query = "SELECT cls.nid as nid, cls.lid as lid, ls.list_name as lname, cls.sid as sid, ss.segment_name as sname
FROM {cm_integrated_campaign_lists} cls
LEFT JOIN {cm_integrated_lists} ls ON (ls.lid = cls.lid)
LEFT JOIN {cm_integrated_segments} ss ON (ss.sid = cls.sid)
WHERE cls.lid IS NOT NULL AND cls.nid IN (" . db_placeholders(array_keys($campaigns), 'varchar') . ")";

    $result = db_query($query, array_keys($campaigns));
    while ($list = db_fetch_array($result)) {
      if ($return_lists) {
        $campaigns[$list['nid']]['lists'][$list['lid']] = $list['lname'];
      }
      if ($return_segements && $list['sid']) {
        $campaigns[$list['nid']]['segments'][$list['sid']] = $list['sname'];
      }
    }
  }

  return $campaigns;
}

/**
 * Update campaign status and send/scheduled date.
 *
 * @param int $nid
 *   Node Id.
 * @param string $date
 *   Campaign send date.
 */
function cm_integrated_db_campaign_send($nid, $date) {
  $status = cm_integrated_utils_status('scheduled');
  db_query("UPDATE {cm_integrated_campaigns} SET send_date = '%s', campaign_status = %d WHERE nid = %d", $date, $status, $nid);
}

/**
 * Fetch campaign statistics from database.
 *
 * @param int $nid
 *   Node Id.
 *
 * @return array
 *   Campaign statistics.
 */
function cm_integrated_db_get_statistics($nid) {
  $query = db_query("SELECT * FROM {cm_integrated_campaigns} cnc LEFT JOIN {cm_integrated_statistics} cns WHERE cnc.nid = %d", $nid);
  return db_fetch_array($query);
}

/**
 * Fetch campaign details from database.
 *
 * @param int $nid
 *   Node Id.
 * @param bool $lists
 *   Include lists?
 * @param bool $segments
 *   Include segments?
 * @param bool $statistics
 *   Include statistics?
 *
 * @return array
 *   Campaign details.
 */
function cm_integrated_db_get_campaign_details($nid, $lists = TRUE, $segments = FALSE, $statistics = FALSE) {
  $campaign = cm_integrated_db_get_campaign(NULL, array('c.nid' => $nid), FALSE, TRUE, FALSE, TRUE);

  if (isset($campaign[$nid])) {
    return $campaign[$nid];
  }
  return array();
}

/**
 * Update campaign statistics.
 *
 * @param array $cm_campaigns
 *   Campaign details from Campaign Monitor.
 * @param bool $update
 *   Update if allready exists?
 */
function cm_integrated_db_update_statistics($cm_campaigns, $update = FALSE) {
  if (empty($cm_campaigns)) {
    return;
  }

  // Statistic fields that we are using currently.
  $fields = array('preview_url', 'recipients');

  $update_campaigns = $values = array();
  if ($update) {
    // Update mode.
    $query = db_query("SELECT cid FROM {cm_integrated_statistics} WHERE preview_url IS NULL AND cid IN (" . db_placeholders(array_keys($cm_campaigns), 'varchar') . ")", array_keys($cm_campaigns));
    while ($result = db_fetch_object($query)) {
      $update_campaigns[$result->cid] = $cm_campaigns[$result->cid];
    }
    if (empty($update_campaigns)) {
      return;
    }
  }
  else {
    // Insert mode.
    $query = db_query("SELECT cid FROM {cm_integrated_campaigns} WHERE cid IN (" . db_placeholders(array_keys($cm_campaigns), 'varchar') . ")", array_keys($cm_campaigns));
    while ($result = db_fetch_object($query)) {
      $update_campaigns[$result->cid] = $cm_campaigns[$result->cid];
    }
  }

  if (empty($update_campaigns)) {
    return;
  }

  $cm_campaigns = $update_campaigns;
  $args = array();
  foreach ($cm_campaigns as $cid => $campaign) {
    $values = $args = array();
    foreach ($fields as $fld) {
      if (isset($campaign[$fld])) {
        if ($fld == 'preview_url') {
          $values[] = "{$fld} = '%s'";
        }
        else {
          $values[] = "{$fld} = %d";
        }
        $args[] = $campaign[$fld];
      }
      else {
        $values[] = "{$fld} = NULL";
      }
    }
    if (!empty($values)) {
      $args[] = $cid;
      db_query("UPDATE {cm_integrated_statistics} SET " . implode(',', $values) . " WHERE cid = %d", $args);
    }
  }
}

/**
 * Delete campaign related records of the node.
 *
 * @param object $node
 *   Node object.
 */
function cm_integrated_db_delete_campaign($node) {
  if (!$node->nid) {
    return;
  }

  $campaign_id = isset($node->cm_integrated) ? $node->cm_integrated['cid'] : NULL;

  if (empty($campaign_id)) {
    $campaign_id = db_result(db_query("SELECT cid FROM {cm_integrated_campaigns} WHERE nid = %d", $node->nid));
  }

  if (!empty($campaign_id)) {
    // Delete statistic record.
    db_query("DELETE FROM {cm_integrated_statistics} WHERE cid = '%s'", $campaign_id);
  }

  // Delete all user lists associated to the node.
  db_query("DELETE FROM {cm_integrated_campaign_lists} WHERE nid = %d", $node->nid);

  // Delete campaign record.
  db_query("DELETE FROM {cm_integrated_campaigns} WHERE nid = %d", $node->nid);
}

/**
 * Helper function to build where clause for given fields.
 */
function _cm_integrated_db_build_where($fields) {
  $where = $args = array();

  foreach ($fields as $field => $value) {
    if (is_array($value) && empty($value)) {
      continue;
    }
    elseif (!is_array($value) && is_null($value)) {
      continue;
    }

    $dbfield = $field;
    if (strpos($field, '.') !== FALSE) {
      $parts = explode('.', $field, 2);
      $dbfield = $parts[1];
    }

    switch ($dbfield) {
      case 'nid':
      case 'campaign_status':
        if (is_array($value)) {
          $where[] = "$field = (%s)";
          $args[] = implode(',', $args);
        }
        else {
          $where[] = "$field = %d";
          $args[] = $value;
        }
        break;

      case 'title':
        $where[] = "$field LIKE '%s'";
        $args[] = "%{$value}%";
        break;
    }
  }

  return array($where, $args);
}

/**
 * Update campaign statistic records.
 *
 * @param array $statistics
 *   Statistic data.
 */
function cm_integrated_db_update_campaign_statistics($statistics) {
  if (empty($statistics['cid'])) {
    return;
  }

  db_query("UPDATE {cm_integrated_statistics} SET preview_url='%s', recipients=%d, opened=%d, clicks=%d, unsubscribed=%d, bounced=%d, unique_opened=%d, stat_update_date='%s' WHERE cid='%s'", $statistics['preview_url'], $statistics['recipients'], $statistics['opened'], $statistics['clicks'], $statistics['unsubscribed'], $statistics['bounced'], $statistics['unique_opened'], date('Y-m-d H:i:s'), $statistics['cid']);
}
