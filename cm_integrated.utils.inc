<?php

/**
 * @file
 * Campaign Monitor - drupal synchronization functions.
 */

/**
 * Validate node type using to create campaign on Campaign Monitor.
 *
 * @param string $type
 *   Node type.
 *
 * @return bool
 *   TRUE for valid node type else FALSE.
 */
function cm_integrated_is_valid_type($type) {
  if (!$type) {
    return FALSE;
  }

  if (($content_type = variable_get('cm_integrated_content_type', 0)) &&
      ($content_type == $type)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Function to provide options for Campaign 'FROM'.
 *
 * @return array
 *   Options for Campaign FROM.
 */
function cm_integrated_options_from() {
  return array(
    'site'   => t('Site name and site email'),
    'author' => t('Author name and email'),
    'other'  => t('As specifed bellow'),
  );
}

/**
 * Function to provide options for Campaign 'REPLY TO'.
 *
 * @return array
 *   Options for Campaign REPLY TO.
 */
function cm_integrated_options_replyto() {
  return array(
    'site'   => t('Site email'),
    'author' => t('Author email'),
    'other'  => t('As specifed bellow'),
  );
}

/**
 * Create html and text format of node body.
 *
 * @param object $node
 *   Node object.
 *
 * @return array
 *   Text file path url,
 *   Html file path url.
 */
function cm_integrated_compose_mail($node) {
  $dir_path = file_directory_path() . '/cm_integrated';
  file_check_directory($dir_path, FILE_CREATE_DIRECTORY);

  // Delete files if already exists.
  cm_integrated_clear_node_files($node);

  if (!empty($node->cm_template)) {
    $content = theme('cm_integrated_template_' . $node->cm_template, $node);
  }
  else {
    $content = $node->body;
  }

  // Html body format.
  $html_file = "cm_{$node->nid}.html";
  $html_file_path = file_create_filename($html_file, $dir_path);
  file_save_data($content, $html_file_path);
  $html_path = file_create_url($html_file_path);

  // Text body format.
  $txt_file = "cm_{$node->nid}.txt";
  $txt_file_path = file_create_filename($txt_file, $dir_path);
  file_save_data(cm_integrated_format_to_text($content), $txt_file_path);
  $txt_path = file_create_url($txt_file_path);

  return array($txt_path, $html_path);
}

/**
 * Delete html and text format files of node body.
 *
 * @param object $node
 *   Node object.
 */
function cm_integrated_clear_node_files($node) {
  if (!$node->nid) {
    return;
  }

  $dir_path = file_directory_path() . '/cm_integrated';
  file_delete($dir_path . "/cm_{$node->nid}.html");
  file_delete($dir_path . "/cm_{$node->nid}.txt");
}

/**
 * Build array used to create campaign on Campaign Monitor.
 *
 * @param object $node
 *   Node object.
 * @param string $txt_path
 *   Text format url of node body.
 * @param string $html_path
 *   Html format url of node body.
 *
 * @return array
 *   Array of parameters used to create campaign.
 */
function cm_integrated_build_campaign_params($node, $txt_path, $html_path) {
  $params = array(
    'campaign_name' => trim($node->title),
    'template'      => empty($node->cm_template) ? NULL : $node->cm_template,
    'nid'           => $node->nid,
    'uid'           => $node->uid,
    'title'         => $node->title,
    'html_url'      => $html_path,
    'text_url'      => $txt_path,
    'lists'         => $node->cm_list,
    'cid'           => '',
    // 'segments' => $node->cm_sengemt
    // Currently segment is disabled.
  );

  list($from_email, $from_name) = _cm_integrated_utils_get_name_email('cm_integrated_from', $node);
  list($reply_to)               = _cm_integrated_utils_get_name_email('cm_integrated_reply_to', $node);

  $params['from_name']  = $from_name;
  $params['from_email'] = $from_email;
  $params['reply_to']   = $reply_to;

  return $params;
}

/**
 * Provide status for campaign.
 *
 * @param string $status_name
 *   Status name.
 * @param bool $by_name
 *   Whether to return status by name or title.
 *
 * @return array
 *   List of status.
 */
function cm_integrated_utils_status($status_name = NULL, $by_name = FALSE) {
  $status = array(
    0 => t('N/A'),
    1 => t('Draft'),
    2 => t('Scheduled'),
    3 => t('Sent'),
  );

  $status_by_name = array(
    'n/a'       => 0,
    'draft'     => 1,
    'scheduled' => 2,
    'sent'      => 3,
  );

  if ($status_name && isset($status_by_name[$status_name])) {
    return $status_by_name[$status_name];
  }

  if ($by_name) {
    return $status_by_name;
  }

  return $status;
}

/**
 * Helper function to retrive email and name used for campaign.
 *
 * @param string $variable
 *   Variable name
 * @param object $node
 *   Node object.
 *
 * @return array
 *   Campaign from/reply to email,
 *   Campaign from name.
 */
function _cm_integrated_utils_get_name_email($variable, $node) {
  $ref = variable_get($variable, 'site');
  if ($ref == 'author') {
    $account = user_load($node->uid);
    return array($account->mail, $account->name);
  }
  elseif ($ref == 'other') {
    foreach (array('email', 'name') as $fld) {
      $$fld = variable_get("{$variable}_{$fld}", '');
    }
    return array($email, $name);
  }
  else {
    $email = variable_get("site_mail", '');
    $name  = variable_get("site_name", '');
    return array($email, $name);
  }
}

/**
 * Helper function to format html data to plain text.
 */
function cm_integrated_format_to_text($data) {
  if (!trim($data)) {
    return $data;
  }
  // All tag <a> to url.
  $pattern = '@<a[^>]+?href="([^"]*)"[^>]*?>(.+?)</a>@is';
  $data = preg_replace_callback($pattern, '_cm_integrated_absolute_urls', $data);

  // Format some special charactors.
  $preg = array(
    '/&quot;/i'  => '"',
    '/&gt;/i'    => '>',
    '/&lt;/i'    => '<',
    '/&amp;/i'   => '&',
    '/&copy;/i'  => '(c)',
    '/&trade;/i' => '(tm)',
    '/&#8220;/'  => '"',
    '/&#8221;/'  => '"',
    '/&#8211;/'  => '-',
    '/&#8217;/'  => "'",
    '/&#38;/'    => '&',
    '/&#169;/'   => '(c)',
    '/&#8482;/'  => '(tm)',
    '/&#151;/'   => '--',
    '/&#147;/'   => '"',
    '/&#148;/'   => '"',
    '/&#149;/'   => '*',
    '/&reg;/i'   => '(R)',
    '/&bull;/i'  => '*',
    '/&euro;/i'  => 'Euro ',
  );

  $data = preg_replace(array_keys($preg), array_values($preg), $data);

  // Drupal html to text conversion.
  return drupal_html_to_text($data);
}

/**
 * Helper function to get absolute url.
 */
function _cm_integrated_absolute_urls($match) {
  global $base_url, $base_path;
  static $regexp;
  $url = $label = '';

  if ($match) {
    if (empty($regexp)) {
      $regexp = '@^' . preg_quote($base_path, '@') . '@';
    }
    list(, $url, $label) = $match;
    $url = strpos($url, '://') ? $url : preg_replace($regexp, $base_url . '/', $url);

    // If the link is formed by Drupal's URL filter, we only return the URL.
    // The URL filter generates a label out of the original URL.
    if (strpos($label, '...') === drupal_strlen($label) - 3) {
      // Remove ellipsis from end of label.
      $label = drupal_substr($label, 0, drupal_strlen($label) - 3);
    }
    if (strpos($url, $label) !== FALSE) {
      return $url;
    }
    return $label . ' ' . $url;
  }
}
