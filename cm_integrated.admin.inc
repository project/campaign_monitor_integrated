<?php

/**
 * @file
 * Admin setting forms.
 * Provide a basic management settings,
 */

/**
 * Campaign setting form.
 */
function cm_integrated_settings($form_state) {
  // Check for API wrapper.
  _cm_integrated_check_wrapper(TRUE);

  $options = array('' => '-select-');
  foreach (node_get_types() as $type => $ctype) {
    $options[$type] = $ctype->name;
  }

  $form['cm'] = array(
    '#type'  => 'fieldset',
    '#title' => t('API Setting'),
  );
  $form['cm']['cm_integrated_api_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('API Key'),
    '#default_value' => variable_get('cm_integrated_api_key', ''),
    '#required'      => TRUE,
  );
  $form['cm']['cm_integrated_client_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Client ID'),
    '#description'   => '<br/>' . t("See Campaign Monitor's <a href='http://www.campaignmonitor.com/api/required/'>api documentation</a> for more info."),
    '#default_value' => variable_get('cm_integrated_client_id', ''),
    '#required'      => TRUE,
  );
  $form['type'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Content Setting'),
  );
  $form['type']['cm_integrated_content_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Content Type'),
    '#default_value' => variable_get('cm_integrated_content_type', ''),
    '#required'      => TRUE,
    '#options'       =>  $options,
  );

  $templates = _cm_integrated_template_files();
  $form['template'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Newsletter Templates'),
  );
  $form['template']['cm_integrated_templates'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select templates available for create newsletter'),
    '#options'       => $templates,
    '#default_value' => variable_get('cm_integrated_templates', array('default', 'teaser')),
  );
  $form['from_setting'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Campaign Setting'),
  );
  $form['from_setting']['cm_integrated_from'] = array(
    '#type'          => 'radios',
    '#title'         => t('Campaign From Name and Email'),
    '#required'      => TRUE,
    '#options'       => cm_integrated_options_from(),
    '#default_value' => variable_get('cm_integrated_from', 'site'),
    '#weigth'        => 1,
  );
  $form['from_setting']['cm_integrated_from_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Name'),
    '#weigth'        => 2,
    '#default_value' => variable_get('cm_integrated_from_name', ''),
    '#size'          => 25,
  );
  $form['from_setting']['cm_integrated_from_email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email'),
    '#weigth'        => 3,
    '#default_value' => variable_get('cm_integrated_from_email', ''),
    '#size'          => 25,
  );
  $form['from_setting']['cm_integrated_reply_to'] = array(
    '#type'          => 'radios',
    '#title'         => t('Reply To Email'),
    '#required'      => TRUE,
    '#options'       => cm_integrated_options_replyto(),
    '#default_value' => variable_get('cm_integrated_reply_to', 'site'),
    '#weigth'        => 4,
  );
  $form['from_setting']['cm_integrated_reply_to_email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email'),
    '#weigth'        => 5,
    '#default_value' => variable_get('cm_integrated_reply_to_email', ''),
    '#size'          => 25,
  );

  $form['#submit'] = array('cm_integrated_settings_submit');

  return system_settings_form($form);
}

/**
 * Validate function for cm_integrated_settings().
 */
function cm_integrated_settings_validate($form, &$form_state) {
  $values = &$form_state['values'];

  if ($values['cm_integrated_from'] == 'other') {
    if (!trim($values['cm_integrated_from_name'])) {
      form_set_error('cm_integrated_from_name', t('Please specify From Name.'));
    }
    else {
      $values['cm_integrated_from_name'] = trim($values['cm_integrated_from_name']);
    }

    if (trim($values['cm_integrated_from_email'])) {
      $values['cm_integrated_from_email'] = trim($values['cm_integrated_from_email']);

      if (!valid_email_address($values['cm_integrated_from_email'])) {
        form_set_error('cm_integrated_from_email', t('The e-mail address %mail is not valid.', array('%mail' => $values['cm_integrated_from_email'])));
      }
    }
    else {
      form_set_error('cm_integrated_from_email', t('Please specify From Email.'));
    }
  }
  else {
    $values['cm_integrated_from_name'] = $values['cm_integrated_from_email'] = '';
  }

  if ($values['cm_integrated_reply_to'] == 'other') {
    if (trim($values['cm_integrated_reply_to_email'])) {
      $values['cm_integrated_reply_to_email'] = trim($values['cm_integrated_reply_to_email']);

      if (!valid_email_address($values['cm_integrated_reply_to_email'])) {
        form_set_error('cm_integrated_reply_to_email', t('The e-mail address %mail is not valid.', array('%mail' => $values['cm_integrated_reply_to_email'])));
      }
    }
    else {
      form_set_error('cm_integrated_reply_to_email', t('Please specify Reply To Email.'));
    }
  }
  else {
    $values['cm_integrated_reply_to_email'] = '';
  }
}

/**
 * Submit function for cm_integrated_settings().
 */
function cm_integrated_settings_submit($form, $form_state) {
  if (!variable_get('cm_integrated_api_key', FALSE)) {
    drupal_set_message(t("Please <a href='@link'> synchronize </a> user list.", array('@link' => url('admin/settings/cm_settings/sync'))));
  }

  // Rebuild theme registry to include new themes.
  drupal_rebuild_theme_registry();
}

/**
 * Campaign and user list synchronization form.
 */
function cm_integrated_synchronize($form_state) {
  $form['userlist'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Synchronize Userlist'),
  );
  $form['userlist']['userlist_help'] = array(
    '#type'  => 'item',
    '#value' => t('Userlist Synchronization will synchronize all users lists from Campaign Moniton, which will availble while creating newsletter.'),
  );
  $form['userlist']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Synchronize Userlist'),
  );
  $form['campaign'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Synchronize Campaigns'),
  );
  $form['campaign']['campaign_help'] = array(
    '#type'  => 'item',
    '#value' => t('Campaign Synchronization will synchronize newsletter with Campaign Monitor and update the newsletter status accordingly.') . '<br/>' . t('Please setup a cron to do this automatically.'),
  );
  $form['campaign']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Synchronize Campaigns'),
  );

  return $form;
}

/**
 * Submit function for cm_integrated_synchronize().
 */
function cm_integrated_synchronize_submit($form, $form_state) {
  require_once dirname(__FILE__) . "/cm_integrated.sync.inc";

  if ($form_state['values']['op'] == t('Synchronize Userlist')) {
    list($messages, $status) = cm_integrated_sync_lists();
    // Segment are disabled for now
    // cm_integrated_sync_segments().
  }
  elseif ($form_state['values']['op'] == t('Synchronize Campaigns')) {
    list($messages, $status) = cm_integrated_sync_campaigns();
  }

  if (isset($status['error']) && !$status['success']) {
    drupal_set_message($status['error'], 'error');
    return;
  }

  if (!empty($messages)) {
    foreach ($messages as $message) {
      drupal_set_message(check_plain($message));
    }
  }
}
