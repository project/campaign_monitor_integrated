<?php
/**
 * @file
 * cm_integrated_content_template_list.tpl.php
 *
 * Theme implementation to display a teaser view
 * for newsletter.
 *
 * Available variables:
 * $nodes list of full node objects
 */
?>
<ul class="cmi_content_wrapper">
<?php
  // $nodes: array of all selected node objects.
  foreach ($nodes as $node) : ?>
    <li><strong><?php echo $node->title; ?></strong><br />
    <?php echo $node->teaser; ?><br />
    <?php echo l(t('Read More'), "node/{$node->nid}"); ?><br/>
    </li>
<?php endforeach; ?>
</ul>
