<?php

/**
 * @file
 * Theme function definitions.
 */

/**
 * Theme for node selection form.
 */
function theme_cm_integrated_content_select($form) {
  if ($form['step']['#value'] == 1) {
    $nids = array();

    // Get selected nodes.
    foreach ($form as $field => $values) {
      if (ereg('nid_', $field)) {
        $nids[] = $values['#return_value'];
      }
    }

    // Table headers.
    $headers = array(
      '',
      array(
        'data'  => t('Nid'),
        'field' => 'nid',
        'sort'  => 'asc',
      ),
      array(
        'data' => t('Title'),
        'field' => 'title',
      ),
      array(
        'data' => t('Created'),
        'field' => 'created',
      ),
      array(
        'data'  => t('Status'),
        'field' => 'status',
      ),
    );

    $nodes = empty($nids) ? array() : _cm_integrated_find_nodes(array('nid' => $nids), $headers);
    $data   = array();
    $status = array(0 => t('not published'), 1 => t('published'));
    foreach ($nodes as $node) {
      $data[] = array(
        drupal_render($form['nid_' . $node->nid]),
        $node->nid,
        $node->title,
        $node->created,
        $status[$node->status],
      );
    }

    if (!empty($nodes)) {
      $table_output = theme('table', $headers, $data);
    }
    else {
      $table_output = '<div>' . t('No any nodes found!') . '</div>';
    }

    return drupal_render($form['search']) . drupal_render($form['campaign_title']) . drupal_render($form['template']) . $table_output . drupal_render($form);
  }
  else {
    return theme('cm_integrated_content_select_sort', $form);
  }
}

/**
 * Theme for node sorting.
 */
function theme_cm_integrated_content_select_sort($form) {
  $headers = array(t('Node title'), t('Weight'));
  $rows    = array();

  foreach ($form as $name => $field) {
    if (ereg('weight_', $name)) {
      $nid    = explode('_', $name);
      $nid    = $nid[1];
      $title  = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $nid));
      $row    = array();
      $row[]  = $title;
      $row[]  = drupal_render($form[$name]);
      $rows[] = array(
        'data' => $row,
        'class' => 'draggable',
      );
    }
  }

  return theme('table', $headers, $rows, array('id' => 'cm_integrated_nodes')) . drupal_render($form);
}
