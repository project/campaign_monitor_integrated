<?php

/**
 * @file
 * Campaign Monitor API call.
 */

/**
 * Provide api settings.
 *
 * @return array
 *   Api client Id and key.
 */
function cm_integrated_get_api() {
  $api = array();
  $api_key = variable_get('cm_integrated_api_key', 0);
  $client_id = variable_get('cm_integrated_client_id', 0);

  if (!_cm_integrated_check_wrapper()) {
    $api['success'] = FALSE;
    $api['error']   = t("We are not able to find API wrapper, please <a href='@link'>download</a> API wapper and put all files in directory '@dir'.", array('@link' => 'https://github.com/campaignmonitor/createsend-php', '@dir' => 'campaignmonitor_createsend'));
    return $api;
  }

  if (!$api_key || !$client_id) {
    $api['success'] = FALSE;
    $api['error']   = t('Missing API setting');
    return $api;
  }

  $api['success']   = TRUE;
  $api['api_key']   = $api_key;
  $api['client_id'] = $client_id;

  return $api;
}

/**
 * Get all user lists from Campaign Monitor.
 *
 * @return array
 *   User lists, Status.
 */
function cm_integrated_get_api_lists() {
  $list = array();
  $api  = cm_integrated_get_api();
  if (!$api['success']) {
    return array($list, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_clients.php";

  $wrap   = new CS_REST_Clients($api['client_id'], $api['api_key']);
  $result = $wrap->get_lists();

  if ($result->was_successful()) {
    $status = array('success' => TRUE);
    if (!empty($result->response)) {
      foreach ($result->response as $responce) {
        $list[$responce->ListID] = $responce->Name;
      }
    }
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($list, $status);
}

/**
 * Get all segments from Campaign Monitor.
 *
 * @return array
 *   Segments, Status.
 */
function cm_integrated_get_api_segments() {
  $segments = array();
  $api = cm_integrated_get_api();
  if (!$api['success']) {
    return array($segments, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_clients.php";

  $wrap   = new CS_REST_Clients($api['client_id'], $api['api_key']);
  $result = $wrap->get_segments();

  if ($result->was_successful()) {
    $status = array('success' => TRUE);
    if (!empty($result->response)) {
      foreach ($result->response as $responce) {
        $segments[$responce->SegmentID] = array(
          'SegmentID' => $responce->SegmentID,
          'ListID'    => $responce->ListID,
          'Title'     => $responce->Title,
        );
      }
    }
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($segments, $status);
}

/**
 * Get all sent campaigns from Campaign Monitor.
 *
 * @return array
 *   Campaigns, Status.
 */
function cm_integrated_api_get_sent_camapigns() {
  $campaigns = array();
  $api = cm_integrated_get_api();
  if (!$api['success']) {
    return array($campaigns, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_clients.php";

  $wrap   = new CS_REST_Clients($api['client_id'], $api['api_key']);
  $result = $wrap->get_campaigns();

  if ($result->was_successful()) {
    $status = array('success' => TRUE);
    if (!empty($result->response)) {
      foreach ($result->response as $responce) {
        $campaigns[$responce->CampaignID] = array(
          'cid'         => $responce->CampaignID,
          'preview_url' => $responce->WebVersionURL,
          'send_date'   => $responce->SentDate,
          'recipients'  => $responce->TotalRecipients,
        );
      }
    }
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($campaigns, $status);
}

/**
 * Get all draft campaigns from Campaign Monitor.
 *
 * @return array
 *   Campaigns, Status.
 */
function cm_integrated_api_get_draft_camapigns() {
  $campaigns = array();
  $api       = cm_integrated_get_api();

  if (!$api['success']) {
    return array($campaigns, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_clients.php";

  $wrap   = new CS_REST_Clients($api['client_id'], $api['api_key']);
  $result = $wrap->get_drafts();

  if ($result->was_successful()) {
    $status = array('success' => TRUE);

    if (!empty($result->response)) {
      foreach ($result->response as $responce) {
        $campaigns[$responce->CampaignID] = array(
          'cid'         => $responce->CampaignID,
          'preview_url' => $responce->PreviewURL,
        );
      }
    }
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($campaigns, $status);
}

/**
 * Create campaigns on Campaign Monitor.
 *
 * @param array $params
 *   Campaign parameters.
 *
 * @return array
 *   Campaign id, Status.
 */
function cm_integrated_api_create_campaign(&$params) {
  $campaign_id = NULL;
  $api         = cm_integrated_get_api();
  if (!$api['success']) {
    return array($campaign, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_campaigns.php";

  if ($params['uid']) {
    $user = user_load($params['uid']);
  }
  else {
    global $user;
  }

  if (!$params['segments']) {
    $params['segments'] = array();
  }

  // @TODO: format $params
  $campaign_params = array(
    "Name"       => $params['campaign_name'],
    "Subject"    => $params['title'],
    "FromName"   => $params['from_name'],
    "FromEmail"  => $params['from_email'],
    "ReplyTo"    => $params['reply_to'],
    "HtmlUrl"    => $params['html_url'],
    "TextUrl"    => $params['text_url'],
    "ListIDs"    => array_values($params['lists']),
    "SegmentIDs" => array_values($params['segments']),
  );

  $wrap   = new CS_REST_Campaigns(NULL, $api['api_key']);
  $result = $wrap->create($api['client_id'], $campaign_params);
  if ($result->was_successful()) {
    $campaign_id = $result->response;
    $status['success'] = TRUE;
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($campaign_id, $status);
}

/**
 * Send draft campaign on Campaign Monitor.
 *
 * @param array $params
 *   Required parameters to send campaign.
 *
 * @return array
 *   Campaign id, Status.
 */
function cm_integrated_api_campaign_send($params) {
  $api = cm_integrated_get_api();
  if (!$api['success']) {
    return array($params['cid'], $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_campaigns.php";

  $wrap = new CS_REST_Campaigns($params['cid'], $api['api_key']);

  $send_data = array(
    'ConfirmationEmail' => $params['send_email'],
    'SendDate' => $params['send_date'],
  );
  $result = $wrap->send($send_data);

  if ($result->was_successful()) {
    $status['success'] = TRUE;
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($params['cid'], $status);
}

/**
 * Delete campaign on Campaign Monitor.
 *
 * @param string $cid
 *   Campaign id.
 *
 * @return array
 *   Campaign id, Status.
 */
function cm_integrated_api_delete_campaign($cid) {
  $api = cm_integrated_get_api();
  if (!$api['success']) {
    return array($cid, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_campaigns.php";

  $wrap   = new CS_REST_Campaigns($cid, $api['api_key']);
  $result = $wrap->delete();

  if ($result->was_successful()) {
    $status['success'] = TRUE;
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($cid, $status);
}

/**
 * Get campaign summary from Campaign Monitor.
 *
 * @param string $cid
 *   Campaign id.
 *
 * @return array
 *   Campaign summary, Status.
 */
function cm_integrated_api_campaign_summary($cid) {
  $api = cm_integrated_get_api();
  if (!$api['success']) {
    return array($cid, $api);
  }

  require_once libraries_get_path('campaignmonitor_createsend') . "/csrest_campaigns.php";

  $wrap   = new CS_REST_Campaigns($cid, $api['api_key']);
  $result = $wrap->get_summary();

  $summary = array();
  if ($result->was_successful()) {
    $status['success'] = TRUE;

    if (!empty($result->response)) {
      $summary = array(
        'cid'         => $cid,
        'preview_url' => $result->response->WebVersionURL,
        'recipients'  => $result->response->Recipients,
        'opened'      => $result->response->TotalOpened,
        'clicks'      => $result->response->Clicks,
        'unsubscribed'  => $result->response->Unsubscribed,
        'bounced'       => $result->response->Bounced,
        'unique_opened' => $result->response->UniqueOpened,
      );
    }
  }
  else {
    $status = array(
      'success'     => FALSE,
      'status_code' => $result->http_status_code,
      'error'       => $result->response,
    );
  }
  return array($summary, $status);
}
