<?php

/**
 * @file
 * Admin setting for content selection.
 */

/**
 * Setting form for Campaign Monitor Integrated Content.
 */
function cm_integrated_content_setting($form_state) {
  // Get all template files.
  $templates = _cm_integrated_content_template_files();
  $form      = array();

  $form['template'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select templates available for create campaign'),
    '#options'       => $templates,
    '#default_value' => variable_get('cm_integrated_content_templates', array('default', 'list')),
    '#required'      => TRUE,
  );
  $form['node_status'] = array(
    '#type'          => 'select',
    '#title'         => t('Default node status'),
    '#options'       => array(
      '0' => t('Unpublished'),
      '1' => t('Published'),
    ),
    '#default_value' => variable_get('cm_integrated_content_node_status', 1),
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit function for cm_integrated_content_setting().
 */
function cm_integrated_content_setting_submit($form, $form_state) {
  $templates = array();
  foreach ($form_state['values']['template'] as $field => $value) {
    if ($value) {
      $templates[] = $field;
    }
  }

  // Template available to create campaign.
  variable_set('cm_integrated_content_templates', $templates);
  // Default node status.
  variable_set('cm_integrated_content_node_status', $form_state['values']['node_status']);
  // Rebuild theme registry to include new themes.
  drupal_rebuild_theme_registry();

  drupal_set_message(t('Setting has been saved.'));
}
